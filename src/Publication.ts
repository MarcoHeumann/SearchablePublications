export class Publication {
  public author: String;
  public title: String;
  public bookTitle: String;
  public year: String;

  public constructor(au: String = "", ti: String = "", bo: String = "", ye: String = "nope") {
    this.author = au;
    this.title = ti;
    this.bookTitle = bo;
    this.year = ye;
  }
}