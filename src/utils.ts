/**
 * Includes commonly used (static) method calls for stuff like sorting, filtering, ect.
 * This is also where possible formatting of tex commands happens.
 */
export default class Utils {

  /**
   * TODO: check if needed
   * Returns a filtered Version of an unfiltered Array<any>.
   * @param array Unfiltered Array of any kind
   * @param filter Filter string(!) to be used
   */
  static nameFilter(array: any[], filter: string) {
    return array.filter(item => item.name.toLowerCase().includes(filter.toLowerCase()));
  }

  /**
   * TODO: check if needed
   * Sorts any form of array by any kind of property. Kind of.
   * @param arr any form of array
   * @param sortBy the property by which is being sorted
   */
  static sort(arr: any[], sortBy: string) {
    return arr.sort((val1, val2): number => {
      if (val1[sortBy] < val2[sortBy]) { return -1; }
      if (val1[sortBy] > val2[sortBy]) { return 1; }
      return 0;
    });
  }

  /**
   * TODO: implement the needed fixes here. Lot's of regex and performance drain here.
   * This function tries to fix whatever is broken. 
   * WILL need adjustments over time, or just make the people follow BibTex rules in the first place.
   * @param arr data collected from the BibTex file
   */
  static fixBibTexData(text: string) {
    //text.split('Johannes').join('Nicht die Mama!');
    text = text.split('{\\"a}').join('{\\"{a}}');
    text = text.split('{\\"u}').join('{\\"{u}}');
    text = text.split('{\\"o}').join('{\\"{o}}');
    text = text.split('AUTHOR').join('author');
    text = text.split('BOOKTITLE').join('booktitle');
    text = text.split('TITLE').join('title');
    return text;
  }

  /**
   * and turns into ,
   */
  static embraceTheComma(text: string) {
    text = text.replace(/ and /g, ", ");
    return text;
  }

  /**
   * Handles regex / conversions of the default data. Leave blank if the default is fine.
   * Add more if further adjustments are wanted.
   * @param text String parameter that represents the Bibtex file
   */
  static unTexThis(text: string) {
    return text;
  }
}