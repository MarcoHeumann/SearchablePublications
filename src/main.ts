// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueMaterial from 'vue-material'
import VueResource from 'vue-resource';
import 'vue-material/dist/vue-material.min.css'
import './hooks' // This must be imported before any component

// REMOVE THIS FOR PRODUCTION
import './styles.css'

import SearchModule from './components/SearchModule.vue'

Vue.use(VueResource)
Vue.use(VueMaterial)

Vue.config.productionTip = false

Vue.component('Settings', {
  components: { SearchModule },
  props: ['username', 'searchbar', 'entries'],
  template: '<SearchModule :username="username" :searchbar="searchbar" :entries="entries"></SearchModule>'
})

new Vue({
  el: '#publications'
})
