# Overview - Searchable Publications

> Search plugin for publications extracted from Bibtek files.  
>   
> Integrates with existing multi page applications, is fully plug and play with minimal imports.  
> Also comes with zero dependencies after deployment.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# generated index.html is for convenient testing only. You don't need it.
# If you want to use it for testing on a production server change assetPublicPath to your needs
config/index.js --> assetsPublicPath = '/mySubdomain/mySubSubdomain/'
```

## Live Implementation

```` bash
# Import publication.js and publication.css on the page that needs the data.
# Inside <head></head> tags for the .css
<link rel="stylesheet" href="/publications.css">
# Just before </body> ending tag is fine for the .js.
<script type="text/javascript" src="/publications.js"></script>

# Tell the page where the results should be rendered and what settings you want to use.
<div id="publications">
    <Settings username="myName" searchbar="true" entries="9001"></Settings>
</div>

# Username needs to be set for personal pages so only publications of that person show up.
default: username=""

# Searchbar is a boolean you can set to include the standard searchbar for publications or not.
# Recommended if you have done more publications then the entries setting show at once.
default: searchbar="true"

# Entries lets you pick how many results should be visible at once.
default: entries="20"

# It is possible to use all, none or any combination of these settings.
# If none are used, the Settings tags need to be included anyways.
# This will lead to a full listing of publications, with a searchbar. Only recommended for the 
# publication overview page, not for personal subpages.
<div id="publications">
    <Settings></Settings>
</div>
````

